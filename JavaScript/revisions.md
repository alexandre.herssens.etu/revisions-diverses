# JS - Révisions

## 1. Introduction 

### A propos

1. Universel (99% de terminaux compatibles)
2. Multi support (navigateur, bureau, serveur, mobile)
3. Standard de l'industrie

### ECMASCRIPT

- spécification suivie par JS
- ES6 : 2015 puis une version par an

#### BABEL

Babel est un compilateur de JS en... JS ! 
En fait, il compile du code JS ES6+ (tout beau tout neuf, avec toutes les 
fonctionnalités ECMAScript qu'on souhaite), en code JS ES5 (tout vieux, 
tout moche mais qui a l'avantage de fonctionner sur presque tous les 
navigateurs) !

### Single Page Application (SPA)

Ne pas recharger la page mais seulement des "morceaux" de page pour une 
meilleure **performance** et **expérience utilisateur**.

1. Action utilisateur (click)
2. JS intercepte puis lance une requête **HTTP** vers le serveur **AJAX**.
3. Serveur retourne les données brutes (JSON, XML, ...)
4. JS parse les données
5. JS modifie le code HTML avec l'**API DOM**

### HTML & JS

La balise script :
- fin du body : parse HTML puis charge JS
- dans le head: HTML (un peu) puis JS puis HTML → ca craint la page ne charge pas
- dans le head + **DEFER** : télécharge le JS dès la balise script et exécute à la 
fin du chargement de la page

### Variables

- ~~var~~
- let
- const

### Types de base

- Faiblement typé : conversion de type implicite
- Dynamique : peut changer de type

#### Conversion de type implicite

JS ne veut pas déclencher d'erreur, il veut que ça tourne.

1. il regarde l'opérateur (+, -, /, *, ...)
2. regarde si l'opérateur est compatible avec les valeurs
3. cherche l'intrus et le convertis

#### Objet littéral

Structure de données / dictionnaire (HashMap)

```js
const toto = {
    id: 1,
    name: "toto",
    hihi : {
        what: true,
        age: 15
    }
};
toto.id;
toto['id'];
toto.hihi.what;
```

##### Destructuring

```js
const character = { firstName: 'Skyler', lastName: 'White', age: 38 };
const { firstName, lastName, age } = character;

function kill(character) {
    const {firstName, lastName} = character;
    console.log( `${firstName} ${lastName} is dead :'(` );
}

function kill( {firstName, lastName} ) { // destructuring directement dans les paramètres !
    console.log( `${firstName} ${lastName} is dead :'(` );
}
```

#### Fonctions
```js
function toto(param){return `what a good ${param}`;} // fonction nommée
const toto = function(param) {return `what a good ${param}`;} // anonyme
const toto = (param) => {return `what a good ${param}`;} // arrow / lambda
const toto = param => `what a good ${param}`;
```

### Chaîne de portée

Référence à une variable :
1. Recherche de la variable dans le bloc de code courant
2. Recherche dans le bloc de code parent...
3. ... et ainsi de suite
4. jusqu'à la portée globale (objet window)
5. Exception de type ReferenceError

## 1.2 - TP

### Console logging
```js
console.warn('Oh oh attention');
console.error('fatal error lol');
console.clear();
console.table([['Walter','White'],['Skyler','White'],['Saul','Goodman']]);
```

### Debugging
```js
const what = 'door';
debugger;
console.log('Hold', 'the', what );
```

### Chaînes de caractères
```js
/* guillemets simples */
const s1 = 'je suis une chaîne avec des single quotes';

/* ou guillemets doubles */
const s2 = "je suis une chaîne avec des double quotes";

/* ou accent grave (template strings ES6) */
const s3 = `Je suis une chaîne avec des accents graves`;
```

#### Concat
```js
// Classique
const lawyer = 'Saul';
const critic = 'better';
console.log('Better call ' + lawyer + ' is ' + critic + ' than Breaking Bad');

// Template
const lawyer = 'Saul';
const critic = 'better';
console.log(`Better call ${lawyer} is ${critic} than Breaking Bad`);
```

## 2. API DOM (Document Object Model)

### Classe Element

Toutes les balises héritent de element

Méthodes :
- selectionner enfants
- modifier contenu html
- detecter les evenements

#### Selectionner
querySelector(All)
```js
const element = document.querySelector('selecteur css');
const elements = document.querySelectorAll('selecteur css'); // []

const input = window.prompt();
```

#### Modifier
```js
element.innerHTML // lire et modifier
element.getAttribute('attr')
element.setAttribute('attr', 'value')
```

#### Evenements
```js
element.addEventListener()
event.currentTarget
event.preventDefault()
```

#### Exemple : formulaire
```js
// 1. Sélectionner le formulaire
const form = document.querySelector('form');
// 2. Détecter l'envoi du formulaire
form.addEventListener('submit', event => {
    event.preventDefault();
    // 3. Sélectionner un champ
    const input = form.querySelector('input[name=message]');
    // 4. Récupérer la valeur saisie par l'utilisateur
    console.log( input.value );
});
```

**Attention** : pas d'ananas sur la pizza

## POO

## Ajax
### Fetch
```js
fetch('api/users')
    .then( response => {
    if (! response.ok ) {
    throw new Error( `Erreur : ${response.statusText}` );
    }
    return response.text();
    })
    .then( responseText => console.log(responseText) )
    .catch( error => alert(error.message) )
```

Pour gérer les erreurs avec fetch, on a plusieurs outils à notre disposition :
- response.ok : true/false
- response.status : code HTTP (200, 400, 404, 500, ...)
- response.statusText : 'OK', 'Bad Request', 'Not found', etc

### JSON
- décoder : JSON.parse()
- encoder : JSON.stringify()

```js
fetch('api/users/todd')
    .then( response => response.text() )
    .then( responseText => JSON.parse(responseText) )
    .then( todd => console.log(todd.treasure) )
```

```js
fetch('api/users/todd')
    .then( response => response.json() )
    .then( todd => console.log(todd.treasure) )
```

### Same Origin Policy
le JS d'un domaine "A"
**NE PEUT PAS ACCÉDER**
aux ressources d'un domaine "B"

### Cors
1. browser lance la requête AJAX : 
    - A demande à B
2. serveur retourne dans sa réponse un header http :
    - B renvoie les données
    - B rajoute un header
```http
Access-Control-Allow-Origin: <domaines-autorisés>
```
3. browser envoie les data au JS ou lance une Exception:
    - Si A est dans la liste, le navigateur affiche les données
    - Sinon Exception


## JQUERY
Bibliothèque qui simplifie :
- la manipulation du DOM,
- la gestion d’événements,
- la création d’animations,
- l’utilisation d’AJAX
- la compatibilité multi-navigateurs

~~Framework~~

### DOM
```js
$('selecteur css').show()
$('selecteur css').attr('style', 'display:block;')

$('selecteur css').find('selecteur css2') 
// multiples classes :  <div class="class1 class2"></div>
//selecteur = 'class1, class2'
```

Le sélecteur magique permet de chaîner les méthodes donc une seule interprétation du sélecteur

### Events
```js
$('selecteur css').on()
$('selecteur css').one() //un event et supprime l'évent

```




## Mots-clefs

| mot               | description                                                                                                                                                                                                                                                     |
|-------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| electron          | Electron est un environnement permettant de développer des applications multi-plateformes de bureau avec des technologies web. L'infrastructure est codée en node.js, et l'interface est bâtie sur les outils Chromium, la partie open source de Google Chrome. |
| npm               | node packet manager                                                                                                                                                                                                                                             |
| npx serve -l 8000 |                                                                                                                                                                                                                                                                 |
| DOM ||

## Cheat sheet

### string
| propriété / méthode   | résultat                       | else |
|:----------------------|:-------------------------------|:----:|
| length                | longeur                        |      |
| search(string)        | indice de début de chaîne      |  -1  |
| split(delimiter)      |                                | [''] |
| substring(debut, fin) |||
| toUpperCase()         |||
| replace(old, new)     | remplace la première occurence ||
| replaceAll(old, new)  | remplace toutes les occurences ||

### array
| propriété / méthode             | résultat                                       | else |
|:--------------------------------|:-----------------------------------------------|:----:|
| length                          |||
| push()                          |||
| join(str)                       |||
| slice(debut, fin?)              | sub array                                      |
| splice(debut, count, ...insert) | retire / remplace des elements dans le tableau |
| sort((a,b) => compare(a,b))     | function de comparaison non obligatoire        ||

| filter(item => condition)       |||

```js
for (const val in array) {doSmtg()};
array.forEach(value => doSmtg());
array.map(value => newValue);

array.reduce( )//aie ma tete
```


### Misc
#### classList

```js
element.classList.add(...class)//class1, class2, class3
element.classList.remove(...class)//class1, class2, class3
element.classList.toggle(class, cond?) //condition
element.classList.replace(class1, class2)//class1, class2, class3
```


- [DATE](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)
- [TP6 soluce](https://gitlab.univ-lille.fr/js/tp6-solution/)




