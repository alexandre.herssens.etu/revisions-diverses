# LE TDD

## En solo 
1. J'écris un test
    - il est rouge
1. J'implémente
    - c'est vert
    - je commit push
1. Je refacto
    - tout est vert
    - je commit push

## Double boucle TDD (TDD Imbriqué)
1. J'écris un test d'acceptance (le produit correspond aux besoins utilisateurs)
    1. J'écris un test unitaire
    1. J'implémente
    1. Je refacto
1. J'implémente
1. Je refacto

Dans une double boucle TDD, je désactive le test de niveau supérieur (*@Disabled*)

## Refacto
- Fier du code?
- Le code respecte les critères de qualité?
- Le code est lisible?
- Le code est propre?

## Résumé 
- Commit et Push quand c'est vert
- 1 Etape = 1/3 du temps
- 1 Test = 1 intention de code
- A la fin d'une boucle, le code est propre

# CRITÈRES DE QUALITÉ

## D'après l'expérience
- DRY ( Don't repeat yourself)
- Simple
- Lisible
- Tous les tests passent

## DRY 
Souvent :
- copier-coller
- fichiers config

### Fichier de config 
**Seulement si** les utilisateurs ne savent pas editer le code

## SIMPLE
- KISS (Keep It Simple Stupid)
- YAGNI (You Ain't Gonna Need It)

## SOLID
### Single Responsability 
une classe, fonction ou méthode = 1 seule responsabilité

### Open-Closed
une classe, fonction ou méthode = fermée à modif mais ouvert à l'extension

### Liskov Substitution
Une sous-classe se comporte comme son parent sans altérer la cohérence du programme

### Interface ségragation
Interfaces spécifiques pour chaque client plutôt qu'une interface générale

### Dépendency inversion
Dépendre des abstractions et non des implémentations

### Déméter

## LISIBLE
- Pas de commentaire
- Moins de 8 lignes / méthode
- Le moins d'opérations mentales possibles

## TESTÉ donc TESTABLE
### Attention
- Reproductibilité ( date du jour)
- Effets de bord
- Pas de sys.out
- Single responsability

### Qualité du test
- Rapide (qq secs)
- Deux executions consécutives et toujours vrai
- Explicite --> nom explicite
- 1 règle métier
- Indépendant des autres tests
- Je peux refacto le code sans modifier le test

- Immutabilité -> faire du given when then

## Given When Then
1. Given : les élements de départ
1. When : quand il se passe quelque chose
1. Then : alors il s'est passé qqchose (assert)

## Résumé
- Précis : pas d'ambiguïté = décrire système avant et après, facile ) reproduire
- Réaliste : vrais exemples (Pizzaiolo luigi = ...)
- Simple : cas précis représentatifs 

## Attentes non fonctionnelles
Attentes utilisateurs comme :
- le service doit répondre en moins de 200 ms
- paramétrable par unique json

Les tests doivent couvrir ces attentes

# Pyramide des tests
- Documenter (générer depuis les tests)
- Faire émerger le code ( critères de qualité)
- Assurer la non régression
- Garantir la sécurité
- Vérifier les performances
- Verifier la résilience 

## Pyramide
### Unitaires
- Par le dév
- Pour le dév

### Acceptance
- Par dév + testeur + métier
- Pour dév + testeur + métier
- Entretenu par dév

    Valider l'histoire utilisateur
    Se mettre à la place de l'utilisateur


### Intégration
- Par dév + testeur + métier
- Pour dév + testeur + métier

    Assemblage des briques et non régression

Simuler des composants distants ou difficilement testables

### Bout en bout
- Par dév + testeur + métier
- Pour dév + testeur + métier

    Agencement des briques
    IHM ou modèle de vue
    Ne rien simuler

### Couverture de test

#### Technique
1. Par ligne de code : chaque ligne sur laquelle un test passe est considérée comme couverte.
1. Par branche ou chemins d’exécution : chaque branche du code dans laquelle un test passe est considérée comme couverte.
#### Fonctionnelle

L’objectif en général est d’avoir 100% sur la couverture fonctionnelle et de dépasser les 80% sur les branches.


# Outils et pratiques













